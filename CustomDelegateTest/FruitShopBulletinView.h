//
//  FruitShopBulletinView.h
//  CustomDelegateTest
//
//  Created by Yinglu ZOU on 10/28/15.
//  Copyright © 2015 snakeninny. All rights reserved.
//

#ifndef FruitShopBulletinView_h
#define FruitShopBulletinView_h


#endif /* FruitShopBulletinView_h */

#import <UIKit/UIKit.h>
#import "FruitShopBulletinViewDelegate.h"

@interface FruitShopBulletinView : UIView
@property UITextView *textView;
@property (nonatomic, weak) id<FruitShopBulletinViewDelegate> delegate;
- (void)setBulletinText:(NSString *)text;
@end