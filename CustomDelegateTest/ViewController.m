//
//  ViewController.m
//  CustomDelegateTest
//
//  Created by Tom Federan on 1/27/15.
//  Copyright (c) 2015 snakeninny. All rights reserved.
//

#import "ViewController.h"

static FruitShop *shop;

@implementation ViewController

@synthesize bulletinView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleFruitsChanged:) name:@"FSFruitCountsChangedNotification" object:nil];
    
    self.bulletinView = [[FruitShopBulletinView alloc] init];
    self.bulletinView.frame = self.view.frame;
    self.bulletinView.delegate = self;
    shop = [FruitShop defaultShop];
    [self.bulletinView setBulletinText:[NSString stringWithFormat:@"Welcome to FruitShop!\r%@ Apples\r%@ Pears\r%@ Oranges", shop.appleCounts, shop.pearCounts, shop.orangeCounts]];
    [self.view addSubview:self.bulletinView];
    
    [self performSelector:@selector(lazyBuyFruits) withObject:nil afterDelay:4.0];
}

- (void)handleFruitsChanged:(NSNotification *)notification
{
    NSLog(@"Model changed: %@ apples, %@ pears and %@ oranges left.", shop.appleCounts, shop.pearCounts, shop.orangeCounts);
    [self.bulletinView setBulletinText:[NSString stringWithFormat:@"In stock\r%@ Apples\r%@ Pears\r%@ Oranges", shop.appleCounts, shop.pearCounts, shop.orangeCounts]];
}

- (void)bulletinViewDidRefresh:(FruitShopBulletinView *)bulletinView
{
    NSLog(@"View changed: Bulletin view did refresh: %@", self.bulletinView.textView.text);
}

- (void)lazyBuyFruits
{
    [self performSelector:@selector(buyApples:) withObject:@2 afterDelay:2.0];
    [self performSelector:@selector(buyPears:) withObject:@4 afterDelay:4.0];
    [self performSelector:@selector(buyOranges:) withObject:@5 afterDelay:6.0];
}

- (void)buyApples:(NSNumber *)apples
{
    [shop sellApples:apples];
}

- (void)buyPears:(NSNumber *)pears
{
    [shop sellPears:pears];
}

- (void)buyOranges:(NSNumber *)oranges
{
    [shop sellOranges:oranges];
}
@end
