//
//  ViewController.h
//  CustomDelegateTest
//
//  Created by snakeninny on 1/27/15.
//  Copyright (c) 2015 snakeninny. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FruitShop.h"
#import "FruitShopBulletinViewDelegate.h"
#import "FruitShopBulletinView.h"

@interface ViewController : UIViewController <FruitShopBulletinViewDelegate>

@property (nonatomic, strong) FruitShopBulletinView *bulletinView;

- (void)lazyBuyFruits;
- (void)handleFruitsChanged:(NSNotification *)notification;
- (void)buyApples:(NSNumber *)apples;
- (void)buyPears:(NSNumber *)pears;
- (void)buyOranges:(NSNumber *)oranges;

@end

