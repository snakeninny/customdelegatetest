//
//  FruitShopBulletinViewDelegate.h
//  CustomDelegateTest
//
//  Created by Yinglu ZOU on 10/28/15.
//  Copyright © 2015 snakeninny. All rights reserved.
//

#ifndef FruitShopBulletinViewDelegate_h
#define FruitShopBulletinViewDelegate_h


#endif /* FruitShopBulletinViewDelegate_h */

@class FruitShopBulletinView;

@protocol FruitShopBulletinViewDelegate <NSObject>

@optional
- (void)bulletinViewDidRefresh:(FruitShopBulletinView *)bulletinView;

@end