//
//  FruitShopBulletinView.m
//  CustomDelegateTest
//
//  Created by Tom Federan on 10/28/15.
//  Copyright © 2015 snakeninny. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FruitShopBulletinView.h"

@implementation FruitShopBulletinView

@synthesize textView;

- (instancetype)init
{
    if ((self = [super init]))
    {
        textView = [[UITextView alloc] init];
        textView.textAlignment = NSTextAlignmentCenter;
        textView.font = [UIFont systemFontOfSize:20];
        textView.textColor = [UIColor redColor];
        [self addSubview:textView];
    }
    return self;
}

- (void)setBulletinText:(NSString *)text
{
    CGFloat centerY = self.frame.origin.y + self.frame.size.height / 3;
    self.textView.frame = CGRectMake(self.frame.origin.x, centerY, self.frame.size.width, self.frame.size.height * 2 / 3);
    self.textView.text = text;
    if (self.delegate && [self.delegate respondsToSelector:@selector(bulletinViewDidRefresh:)]) [self.delegate bulletinViewDidRefresh:self];
}
@end