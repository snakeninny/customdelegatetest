//
//  FruitShop.m
//  CustomDelegateTest
//
//  Created by snakeninny on 1/27/15.
//  Copyright (c) 2015 snakeninny. All rights reserved.
//

#import "FruitShop.h"

static FruitShop *defaultShop;

@implementation FruitShop

@synthesize appleCounts;
@synthesize pearCounts;
@synthesize orangeCounts;

+ (void)initialize
{
    if (self == [FruitShop class]) defaultShop = [[self alloc] init];
}

+ (instancetype)defaultShop
{
    return defaultShop;
}

- (instancetype)init
{
    if ((self = [super init]))
    {
        appleCounts = @10;
        pearCounts = @10;
        orangeCounts = @10;
    }
    return self;
}

- (void)postFruitsChangeNotification
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"FSFruitCountsChangedNotification" object:nil];
}

- (void)sellApples:(NSNumber *)apples
{
    self.appleCounts = @([self.appleCounts unsignedIntegerValue] - [apples unsignedIntegerValue]);
    [self postFruitsChangeNotification];
}

- (void)sellPears:(NSNumber *)pears
{
    self.pearCounts = @([self.pearCounts unsignedIntegerValue] - [pears unsignedIntegerValue]);
    [self postFruitsChangeNotification];
}

- (void)sellOranges:(NSNumber *)oranges
{
    self.orangeCounts = @([self.orangeCounts unsignedIntegerValue] - [oranges unsignedIntegerValue]);
    [self postFruitsChangeNotification];
}

@end
