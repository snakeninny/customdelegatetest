//
//  AppDelegate.h
//  CustomDelegateTest
//
//  Created by snakeninny on 1/27/15.
//  Copyright (c) 2015 snakeninny. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

