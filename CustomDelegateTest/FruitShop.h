//
//  FruitShop.h
//  CustomDelegateTest
//
//  Created by snakeninny on 1/27/15.
//  Copyright (c) 2015 snakeninny. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FruitShop : NSObject

@property (nonatomic, assign) NSNumber * appleCounts;
@property (nonatomic, assign) NSNumber * pearCounts;
@property (nonatomic, assign) NSNumber * orangeCounts;

+ (instancetype)defaultShop;
- (void)sellApples:(NSNumber *)apples;
- (void)sellPears:(NSNumber *)pears;
- (void)sellOranges:(NSNumber *)oranges;
- (void)postFruitsChangeNotification;

@end